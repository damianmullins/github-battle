var React = require('react');
var ReactDOM = require('react-dom');
var routes = require('./config/routes');
var Raven = require('raven-js');

var sentryKey = '44b3f3a09ec843c886a23edd03a33d67';
var sentryApp = '94923';
var sentryURL = ['https://', sentryKey, '@app.getsentry.com/', sentryApp].join('');

var APP_INFO = {
  name: 'Github Battle',
  branch: 'master',
  version: '1.0.1'
};

Raven.config(sentryURL, {
  release: APP_INFO.version,
  tags: {
    branch: APP_INFO.branch
  }
}).install();

ReactDOM.render(
  routes,
  document.getElementById('app')
);
